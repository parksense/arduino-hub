#include <GSM.h>
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"
#include <Time.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"

// GSM config
GSMClient client;
GPRS gprs;
GSM gsmAccess;
String IMEI = "013950004039609";
#define PINNUMBER "1597"
char server[] = "api-parksense.herokuapp.com/v1/sensor/occupation?value="; // the base URL
int port = 80;

int running = 0;
boolean mutexRunning = false;

// Park count occupation
int parkOcc = 0;

int senderId;
RF24 radio(9,10);

struct config_t
{
  byte pipe; 
  byte mode;
  byte config_set;
  byte slaves;
  char slaveWebIds[2][25];
  int slaveIds[50];
  int slaveOccupations[50];
} eeprom_config;

void set_EEPROM_Default() {
    eeprom_config.pipe = 1;
    eeprom_config.mode = 1;
    eeprom_config.config_set=1;
    eeprom_config.slaves = 2;
    eeprom_config.slaveIds[0] = 99;
    eeprom_config.slaveIds[1] = 98;
    strcpy(eeprom_config.slaveWebIds[0], "576432280530fd7b2ff81021");
    strcpy(eeprom_config.slaveWebIds[1], "576432280530fd7b2ff81021");
    eeprom_config.slaveOccupations[0] = 00;
    eeprom_config.slaveOccupations[1] = 00;
}

void read_EEPROM_Settings() {
  //INSERIR || NO IF PARA QUANDO É PRESSIONADO O BOTAO DE RESET
  
  // read the current config
  EEPROM_readAnything(0, eeprom_config);
  
  if (eeprom_config.config_set < 1) {
    // set default values
    printf("Setting values...\r\n");
    set_EEPROM_Default();
    
    // write the config to eeprom
    EEPROM_writeAnything(0, eeprom_config);
  } 
}

void print_EEPROM_Settings() {
  Serial.print("pipe: ");
  Serial.println((long)get_pipe(eeprom_config.pipe));
  Serial.print("mode: ");
  Serial.println(eeprom_config.mode);
  Serial.print("config set: ");
  Serial.println(eeprom_config.config_set);
  Serial.print("slaves: ");
  Serial.println(eeprom_config.slaves);
  for(int i = 0; i<eeprom_config.slaves; i++) {
    Serial.print("slave id: ");
    Serial.println(eeprom_config.slaveIds[i]);
    Serial.print("slave occupation: ");
    Serial.println(eeprom_config.slaveOccupations[i]);
  }
}

uint64_t get_pipe(char pipe){
  switch (pipe) {
    case 1:
      return 0xABCDABCD71LL;
      break;
    case 2:
      return 0xF0F0F0F0D2LL;
      break;
    case 3:
      return 0xF0F0F0F0C3LL;
      break;
    case 4:
      return 0xF0F0F0F0B4LL;
      break;
    case 5:
      return 0xF0F0F0F0A5LL;
      break;
    default: 
      return 0xF0F0F0F096LL;
      break;
  }
}

void open_radio_Settings(){
  radio.begin();
  radio.setRetries(15,15);
  radio.openWritingPipe(get_pipe(eeprom_config.pipe));
  radio.openReadingPipe(1,get_pipe(eeprom_config.pipe));
  radio.startListening();
}

void setupHub() {
  read_EEPROM_Settings();
  print_EEPROM_Settings();
  open_radio_Settings();

  // connection state
  boolean notConnected = true;

  // Start GSM shield
  // pass the PIN of your SIM as a parameter of gsmAccess.begin()
  while(notConnected)
  {
    if(gsmAccess.begin(PINNUMBER)==GSM_READY)
      notConnected = false;
    else
    {
      Serial.println("Not connected");
      delay(1000);
    }
  }
}

void setup(void)
{
  Serial.begin(9600);
  printf_begin();
  printf("\n\rHUB\n\r");

  delay(200); // some time to settle
  setupHub();
  delay(200); // some time to settle
}

void loop(void)
{
  receive();
  sendTimer(); 
}

void receive() {
  if ( radio.available() )
  {
    bool done = false;
    while (!done)
    {
      done = radio.read(&senderId, 4);
      printf("Got payload %d...\n\r",senderId);
      storeCount(senderId);
    }
  }
}

void updateHTTPServer(char* idSensor, int count) {
  Serial.println("connecting...");
  char outputURL[80];
  sprintf(outputURL,"%s%i&id=%s",server,count,idSensor);
  if (client.connect(server, port))
  {
    Serial.println("connected");
    client.print("PUT ");
    client.print(outputURL);
    client.println(" HTTP/1.0");
    client.println();
  } 
  else
  {
    // if you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void storeCount(int message) {
  String aux = String(message);
  int from = aux.substring(0, 2).toInt();
  int count = aux.substring(2, 4).toInt();
  for (int i=0; i<eeprom_config.slaves; i++){
    if (eeprom_config.slaveIds[i] == from) {
      eeprom_config.slaveOccupations[i] += count;
    }
  }
}


void sendTimer() {
  if(second() == 2) {
    mutexRunning = true;
  } else {
    mutexRunning = false;
    running = 0;
  }
  if(mutexRunning && running == 0) {
    running = 1;
    send();
  }
}

boolean send()
{
  if(running == 1) {
    for (int i=0; i<eeprom_config.slaves; i++){
      updateHTTPServer(eeprom_config.slaveWebIds[i], eeprom_config.slaveOccupations[i]);
    }
  }
}
